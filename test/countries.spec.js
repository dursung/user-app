const axios = require("axios");

const getCountries = require("../src/routes/api/countries");
const counrtyBaseUrl = require("../src/config/key_dev").counrtyBaseURL;

jest.mock("axios");

describe("fetchCountries", () => {
  it("fetches successfully data from an API", async () => {
    const response = {
      data: [
        {
          name: "UK",
          topLevelDomain: [".ax"],
          alpha2Code: "AX",
          alpha3Code: "ALA",
        },
      ],
    };

    axios.get.mockImplementationOnce(() =>
      Promise.resolve(JSON.stringify(response))
    );

    await expect(getCountries()).resolves.toEqual(JSON.stringify(response));

    expect(axios.get).toHaveBeenCalledWith(`${counrtyBaseUrl}/region/Europe`);
  });
});
