const axios = require("axios");
const mongoBaseUrl = require("../../config/key_dev").mongoBaseURL;

const post = async (data) => {
  let url = mongoBaseUrl + "/users";

  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  let isError = false;
  let errors = [];
  const response = await axios
    .post(url, data, config)
    .then((response) => response)
    .catch((error) => {
      isError = true;
      errors.api = error;
      console.log(error);
    });

  return { isError, errors, response };
};

module.exports = post;
