const axios = require("axios");
const counrtyBaseUrl = require("../../config/key_dev").counrtyBaseURL;

const getCountries = async () => {
  const response = await axios
    .get(`${counrtyBaseUrl}/region/Europe`)
    .then((response) => response)
    .catch((error) => console.log(error));

  return response;
};

module.exports = getCountries;
