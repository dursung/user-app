const express = require("express");
const router = express.Router();
const post = require("../api/post");
const countries = require("../api/countries");
const validateRegisterInput = require("../../validation/register");

router.get("/", async (req, res, next) => {
  const dataCountries = await countries();
  let data = {
    layout: "layout.njk",
    title: "Registration",
    countries: dataCountries.data.map((country) => country.name),
  };

  res.render("registration.njk", data);
});

router.post("/", async (req, res, next) => {
  const data = {
    name: req.body.name,
    sex: req.body.sex,
    age: req.body.age,
    country: req.body.country,
  };
  validateRegisterInput(data);
  const { errors, isValid } = validateRegisterInput(req.body);
  // Check Validation
  if (!isValid) {
    const dataCountries = await countries();
    let data = {
      layout: "layout.njk",
      title: "Registration",
      countries: dataCountries.data.map((country) => country.name),
      errors,
    };
    res.render("registration.njk", data);
  } else {
    const { isError, errors } = await post(data);
    const dataCountries = await countries();

    if (isError) {
      let data = {
        layout: "layout.njk",
        title: "Registration",
        countries: dataCountries.data.map((country) => country.name),
        errors,
      };
      res.render("registration.njk", data);
    } else {
      let pageData = {
        layout: "layout.njk",
        title: "Home",
        name: req.body.name,
      };
      res.render("home.njk", pageData);
    }
  }
});

module.exports = router;
