# User Registration

Useful Government Service

---

## Requirements

### Node

- #### Node installation
  Just go on [official Node.js website](https://nodejs.org/) and download the installer.

## Install Project

    $ git clone https://gitlab.com/dursung/user-app.git
    $ cd user-app
    $ npm install

## Testing the project

    $ npm run test

## Running the project

    $ npm start

### Local server url : http://localhost:3000/

## Screenshots
<table style={border:"none"}><tr><td><img src="https://gitlab.com/dursung/user-app/-/blob/7d0e8e8b24df45981f749b580e6ab46bfc8755d1/screens/1.png" alt="Landing Page" width="400"/></td><td><img src="https://gitlab.com/dursung/user-app/-/blob/7d0e8e8b24df45981f749b580e6ab46bfc8755d1/screens/2.png" alt="Secondary Home Page" width="400"/></td></tr></table>