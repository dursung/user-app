const express = require("express");
const nunjucks = require("nunjucks");
const logger = require("morgan");
const path = require("path");
const bodyParser = require("body-parser");

const usersRouter = require("./src/routes/users");
const countries = require("./src/routes/api/countries");

const app = express();

nunjucks.configure(path.resolve(__dirname, "src/views"), {
  autoescape: true,
  express: app,
});

app.use(logger("dev"));
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "src/assets")));

app.use("/", usersRouter);
app.use("/api/countries", countries);

app.listen(3000, () => {
  console.log("express server running on ", 3000);
});
